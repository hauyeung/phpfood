<html>
<body>
<?php

//assign beverage according to food
$beverage = "";
$food = $_GET['food'];

$steakprice = 8;
$salmonprice = 7;
$porkprice = 9;
$chickenprice = 10;

/*drink prices*/
$water= 1;
$soda= 2;
$juice= 3;
$tea = 1.5;

$foodprice = 0;
$drinkprice = 0;

switch ($food)
{
case "steak":
$foodprice = $steakprice;
$drinkprice = $water;
$beverage = "water";
$subtotal  = $steakprice+$water;
break;

case "barbeque pork":
$foodprice = $porkprice;
$drinkprice = $soda;
$beverage = "soda";
$subtotal = $porkprice+$soda;
break;

case "chicken":
$foodprice = $chickenprice;
$drinkprice = $juice;
$beverage = "orange juice";
$subtotal = $chickenprice+$juice;
break;

case "salmon":
$foodprice = $salmonprice;
$drinkprice = $tea;
$beverage = "tea";
$subtotal = $salmonprice+$tea;
break;
}

/*tax and tip*/
$tax = 0.08;
$tip = 0.15;

$grandtotal = $subtotal*(1+$tax+$tip);
/* print food  and drink */
print "Your food is: ".$food." - $".$foodprice."<br>";
print "Your drink is: ".$beverage." - $".$drinkprice."<br>";
/* print subtotal, tax, and tip */
print "Subtotal: ".$subtotal."<br> tax: $".$subtotal*$tax."<br> tip: $".$subtotal*$tip."<br>";
/* print grand total */
print "Grand total: $".$grandtotal;

?>

</body>
</html>