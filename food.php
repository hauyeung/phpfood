<html>
<body>
<?php 
$subtotal  = 0;
$food = $_GET['food'];
$drink  = "";
/*food prices*/
$chickenprice = 5;
$steakprice = 6;
$salmonprice = 7;
$porkprice = 8;

/*drink prices*/
$water= 1;
$soda= 2;
$juice= 3;
$tea = 4;

$foodprice =0;
$drinkprice = 0;

/* set prices are food and drink */
if ($food=="steak")
{
$drink = "water";
$foodprice = $steakprice;
$drinkprice = $water;
$subtotal = $steakprice+$water;
}
else if ($food=="chicken")
{
$foodprice = $chickenprice;
$drink = "soda";
$drinkprice = $soda;
$subtotal = $chickenprice+$soda;
}
else if ($food =="salmon")
{
$foodprice = $salmonprice;
$drink = "juice";
$drinkprice = $juice;
$subtotal = $salmonprice+$juice;
}
else if ($food=="barbeque pork")
{
$foodprice = $porkprice;
$drinkprice = $tea;
$drink = "tea";
$subtotal = $porkprice+$tea;
}



/*tax and tip*/
$tax = 0.08;
$tip = 0.15;

$grandtotal = $subtotal*(1+$tax+$tip);

/*display items chosen, prices and totals */
print $food." - $".$foodprice."<br>";
print $drink." - $".$drinkprice."<br>";
print "Subtotal: ".$subtotal."<br> tax: $".$subtotal*$tax."<br> tip: $".$subtotal*$tip."<br>";
print "Grand total: $".$grandtotal;

?>
</body>
</html>